'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'compareTriplets' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY a
 *  2. INTEGER_ARRAY b
 */

function compareTriplets(a, b) {
    // ariabel alice dan bob untuk menyimpan poin masing-masing
    let alice = 0;
    let bob = 0;
    // Looping dan menyimpan index perulangan dalam variabel i
    for (const i in a) {
    // apakah nilai Alice dan Bob berbeda atau tidak
        if (a[i] != b[i]) {
    // membandingkan nilai Alice dan Bob
    // Apabila nilai Alice lebih besar dari nilai Bob maka poin Alice ditambah 1, begitu juga sebaliknya.
            a[i] > b[i] ? alice++ : bob++;
        }
    }
    // return nilai alice dan bob
    return [alice, bob];
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const a = readLine().replace(/\s+$/g, '').split(' ').map(aTemp => parseInt(aTemp, 10));

    const b = readLine().replace(/\s+$/g, '').split(' ').map(bTemp => parseInt(bTemp, 10));

    const result = compareTriplets(a, b);

    ws.write(result.join(' ') + '\n');

    ws.end();
}
